import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptor implements HttpInterceptor{
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const data = window.localStorage.getItem('data'); //todo: null abfangen

    if (data && req.url.includes('backend')) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${JSON.parse(data).access_token}`
        },
        withCredentials: true
      });
    }
    return next.handle(req);
  }
}
