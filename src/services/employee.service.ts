import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Employee } from "../app/Employee";
import { MessageService } from "./message.service";
import {Qualification} from "../app/Qualification";
import {SkilledEmployee} from "../app/SkilledEmployee";

@Injectable({ providedIn: 'root' })
export class EmployeeService {

  private employeeUrl = '/backend/employees';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) {
    this.employees$ = of([]);
    this.employee$ = of()
    this.qualification$ = of([]);
  }

  employees$: Observable<Employee[]>;
  employee$: Observable<Employee>;
  qualification$: Observable<Qualification[]>

  getEmployee(id: number): Observable<Employee> {
    return this.http.get<Employee>(this.employeeUrl+`/${id}`).pipe(
      tap(_ => this.log(`fetched employee id=${id}`)),
      catchError(this.handleError<Employee>(`getEmployee id=${id}`))
    );
  }

  getEmployeeQualification(id: number): Observable<SkilledEmployee> {
    return this.http.get<SkilledEmployee>(this.employeeUrl+`/${id}/qualifications`).pipe(
      tap(_ => this.log(`fetched employee id=${id}`)),
      catchError(this.handleError<SkilledEmployee>(`getEmployee id=${id}`))
    );
  }

  /** GET employees from the server */
  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.employeeUrl)
      .pipe(
        tap(_ => this.log('fetched employees')),
        catchError(this.handleError<Employee[]>('getEmployees', []))
      );
  }

  addQualificationToEmployee(designation: string | undefined, id: number): Observable<SkilledEmployee>{
    let requestBody: string = JSON.stringify({"designation": designation})
    return this.http.post<SkilledEmployee>(this.employeeUrl+`/${id}/qualifications`, requestBody, this.httpOptions).pipe(
      tap((newQualification: SkilledEmployee) => this.log(`added qualification`)),
      catchError(this.handleError<SkilledEmployee>('addQualificationToEmployee'))
    );
  }

  deleteQualificationFromEmployee(designation: string | undefined, id: number): Observable<SkilledEmployee | ArrayBuffer>{
    let requestBody: string = JSON.stringify({"designation": designation})
    return this.http.delete<SkilledEmployee>(this.employeeUrl+`/${id}/qualifications`, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      body: requestBody
    }).pipe(
      tap(_ => this.log(`deleted qualification from employee with id=${id}`)),
      catchError(this.handleError<SkilledEmployee>(`deleteQualificationFromEmployee id=${id}`))
    );
  }

  /** GET employees whose name contains search term */
  searchEmployees(term: string): Observable<Employee[]> {
    if (!term.trim()) {
      // if not search term, return empty employee array.
      return of([]);
    }
    return this.http.get<Employee[]>(`${this.employeeUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
        this.log(`found employee matching "${term}"`) :
        this.log(`no employee matching "${term}"`)),
      catchError(this.handleError<Employee[]>('searchEmployee', []))
    );
  }

  //////// Save methods //////////

  /** POST: add a new employee to the server */
  addEmployee(employee: Employee): Observable<Employee> {
    return this.http.post<Employee>(this.employeeUrl, employee, this.httpOptions).pipe(
      tap((newEmployee: Employee) => this.log(`added employee w/ id=${newEmployee.id}`)),
      catchError(this.handleError<Employee>('addEmployee'))
    );
  }

  /** DELETE: delete the employee from the server */
  deleteEmployee(id: number | undefined): Observable<Employee>{
    return this.http.delete<Employee>(`/backend/employees/${id}`, this.httpOptions).pipe(
      tap(_ => this.log(`deleted employee id=${id}`)),
      catchError(this.handleError<any>('deleteEmployee'))
    );
  }

  /** PUT: update the employee on the server */
  updateEmployee(employee: Employee): Observable<Employee> {
    return this.http.put(`/backend/employees/${employee.id}`, employee, this.httpOptions).pipe(
      tap(_ => this.log(`updated employee id=${employee.id}`)),
      catchError(this.handleError<any>('updateEmployee'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   *
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a EmployeeService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`EmployeeService: ${message}`);
  }
}
