import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient) {

  }

  login(username: string, password: string) {
    const headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
    const body = new URLSearchParams();
    body.set('grant_type', 'password');
    body.set('client_id', 'employee-management-service');
    body.set('username', username);
    body.set('password', password);

    return this.httpClient.post<any>('http://authproxy.szut.dev', body, { headers });
  }
}
