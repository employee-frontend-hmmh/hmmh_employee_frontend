import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {MessageService} from "./message.service";
import {Observable, of} from "rxjs";
import {Qualification} from "../app/Qualification";
import {catchError, tap} from "rxjs/operators";
import {AppComponent} from "../app/app.component";

@Injectable({
  providedIn: 'root'
})
export class QualificationService {

  private qualificationUrl = '/backend/qualifications';  // URL to web api
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private appComponent: AppComponent,
    private messageService: MessageService) {
    this.qualifications$ = of([]);
    this.qualification$ = of();
  }

  qualifications$: Observable<Qualification[]>
  qualification$: Observable<Qualification>

  /** GET qualifications from the server */
  getQualifications(): Observable<Qualification[]> {
    return this.http.get<Qualification[]>(this.qualificationUrl)
      .pipe(
        tap(_ => this.log('fetched qualifications')),
        catchError(this.handleError<Qualification[]>('getQualifications', []))
      );
  }

  addQualification(qualification : Qualification): Observable<Qualification> {
    return this.http.post<Qualification>(this.qualificationUrl, qualification, this.httpOptions).pipe(
      tap((newQualification: Qualification) => this.log(`added qualification`)),
      catchError(this.handleError<Qualification>('addQualification'))
    );
  }

  deleteQualification(designation: string | undefined): Observable<Qualification>{
    let requestBody: string = JSON.stringify({"designation": designation})
    return this.http.delete<Qualification>(this.qualificationUrl, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      body: requestBody
    }).pipe(
      tap(_ => this.log(`deleted qualification`)),
      catchError(this.handleError<any>('addQualification'))
    );
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   *
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a QualificationService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`QualificationService: ${message}`);
  }
}
