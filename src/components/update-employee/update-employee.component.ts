import { Component, OnInit } from '@angular/core';
import {EmployeeService} from "../../services/employee.service";
import {Employee} from "../../app/Employee";
import {Observable, of} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";
import {SkilledEmployee} from "../../app/SkilledEmployee";
import {Qualification} from "../../app/Qualification";
import {QualificationService} from "../../services/qualification.service";

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {

  employee: Employee | undefined;
  employeeQualification$: Observable<SkilledEmployee> | undefined;
  qualifications$: Observable<Qualification[]>;
  id = Number(this.route.snapshot.paramMap.get('id'));
  selectedValue: any;

  constructor(private route: ActivatedRoute,
              private employeeService: EmployeeService,
              private qualificationService: QualificationService,
              private router: Router,
  ) {
    this.getEmployee();
    this.getEmployeeQualification();
    this.qualifications$ = of([]);
    this.getQualifications();
  }

  ngOnInit(): void {
  }

  getEmployee() {
    this.employeeService.getEmployee(this.id)
      .subscribe(employee => this.employee = employee);
  }

  getEmployeeQualification(){
    this.employeeQualification$ = this.employeeService.getEmployeeQualification(this.id)
  }

  update(id: number | undefined, firstName: string, lastName: string, street: string, postcode: string, city: string, phone: string): void {
    firstName = firstName.trim();
    lastName = lastName.trim();
    street = street.trim();
    postcode = postcode.trim();
    city = city.trim();
    phone = phone.trim();
    if(!lastName || !firstName || !street || !postcode || !city || !phone){return;}
    this.employeeService.updateEmployee({id, firstName, lastName, street, postcode, city, phone} as Employee).subscribe();
    this.router.navigate(['/'])
  }

  getQualifications(){
    this.qualifications$ = this.qualificationService.getQualifications();
  }

  addQualification(designation: string | undefined){
    this.employeeService.addQualificationToEmployee(designation, this.id).subscribe();
    window.location.reload();
  }

  deleteQualification(designation: string | undefined){
    this.employeeService.deleteQualificationFromEmployee(designation, this.id).subscribe();
    window.location.reload();
  }

}
