import { Component, OnInit } from '@angular/core';
import {LoginService} from "../../services/login.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private loginService: LoginService, private router: Router) {
  }

  ngOnInit(): void {
  }

  login(username: string, password: string) {
    this.loginService.login(username, password).subscribe(data => {
      window.localStorage.setItem('data', JSON.stringify(data));
    });
    this.router.navigate(['/'])
  }

  register(username: string, mail: string, password: string) {
    console.log(username);
    console.log(mail);
    console.log(password);
  }
}
