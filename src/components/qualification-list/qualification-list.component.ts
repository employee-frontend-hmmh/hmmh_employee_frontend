import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {Qualification} from "../../app/Qualification";
import {QualificationService} from "../../services/qualification.service";

@Component({
  selector: 'app-qualification-list',
  templateUrl: './qualification-list.component.html',
  styleUrls: ['./qualification-list.component.css']
})
export class QualificationListComponent implements OnInit {
  qualifications$: Observable<Qualification[]>;

  constructor(
    private http: HttpClient,
    private qualificationService: QualificationService) {
    this.qualifications$ = of([]);
    this.getQualifications();
  }

  getQualifications() {
    this.qualifications$ = this.qualificationService.getQualifications();
  }
  deleteQualification(designation: string | undefined){
    this.qualificationService.deleteQualification(designation).subscribe();
    window.location.reload();
  }
  ngOnInit(): void {
  }
}
