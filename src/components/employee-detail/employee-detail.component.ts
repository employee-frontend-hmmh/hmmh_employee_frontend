import {Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import {EmployeeService} from "../../services/employee.service";
import {Employee} from "../../app/Employee";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {Qualification} from "../../app/Qualification";
import {SkilledEmployee} from "../../app/SkilledEmployee";

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {
  employee: Employee | undefined;
  skilledEmployee: SkilledEmployee | undefined;
  id = Number(this.route.snapshot.paramMap.get('id'));
  employeeQualification$: Observable<SkilledEmployee> | undefined;

  ngOnInit() {
    this.getEmployee()
  }
  constructor(private route: ActivatedRoute,
              private employeeService: EmployeeService,
              private location: Location,
  ) {
    this.getEmployee();
    this.getEmployeeQualification();
  }
  getEmployee() {
    this.employeeService.getEmployee(this.id)
      .subscribe(employee => this.employee = employee);
  }

  getEmployeeQualification(){
    this.employeeQualification$ = this.employeeService.getEmployeeQualification(this.id)
  }

  goBack(): void {
    this.location.back();
  }
}

