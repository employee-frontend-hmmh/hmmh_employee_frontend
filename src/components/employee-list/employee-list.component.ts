import { Component, OnInit } from '@angular/core';
import {Observable, of} from "rxjs";
import {Employee} from "../../app/Employee";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {EmployeeService } from "../../services/employee.service";

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  ngOnInit(): void {
  }employees$: Observable<Employee[]>;

  constructor(private http: HttpClient, private employeeService: EmployeeService) {
    this.employees$ = of([]);
    this.getEmployees();
  }

  getEmployees(){
    this.employees$ = this.employeeService.getEmployees();
  }

  delete(id: number | undefined) {
    this.employeeService.deleteEmployee(id).subscribe();
    window.location.reload();
  }
}
