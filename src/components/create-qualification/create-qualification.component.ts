import { Component, OnInit } from '@angular/core';
import {QualificationService} from "../../services/qualification.service";
import {Qualification} from "../../app/Qualification";

@Component({
  selector: 'app-create-qualification',
  templateUrl: './create-qualification.component.html',
  styleUrls: ['./create-qualification.component.css']
})
export class CreateQualificationComponent implements OnInit {

  constructor(private qualificationService: QualificationService) { }

  ngOnInit(): void {
  }

  createQualification(designation: string){
    designation.trim()
    if(!designation){return;}
    this.qualificationService.addQualification({designation} as Qualification).subscribe();
    window.location.reload();
  }
}
