import { Component, OnInit } from '@angular/core';
import {EmployeeService} from "../../services/employee.service";
import {Employee} from "../../app/Employee";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  constructor(private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
  }

  create(lastName: string, firstName: string, street: string, postcode: string, city: string, phone: string): void {
    lastName = lastName.trim();
    firstName = firstName.trim();
    street = street.trim();
    postcode = postcode.trim();
    city = city.trim();
    phone = phone.trim();
    if(!lastName || !firstName || !street || !postcode || !city || !phone){return;}
    this.employeeService.addEmployee({lastName,firstName, street, postcode, city, phone} as Employee).subscribe();
    this.router.navigate(['/'])
  }

}
