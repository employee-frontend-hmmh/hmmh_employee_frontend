import {Qualification} from "./Qualification";

export class SkilledEmployee {
  constructor(public id?: number,
              public surname?: string,
              public firstname?: string,
              public skillSet?: Qualification[]) {
  }
}
