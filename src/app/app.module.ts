import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { NavigationComponent } from '../components/navigation/navigation.component';
import { ControlPanelComponent } from '../components/control-panel/control-panel.component';
import { EmployeeListComponent } from '../components/employee-list/employee-list.component';
import { QualificationListComponent } from '../components/qualification-list/qualification-list.component';
import { LoginComponent } from '../components/login/login.component';
import {TokenInterceptor} from "../interceptors/token-interceptor";
import { EmployeeDetailComponent } from '../components/employee-detail/employee-detail.component';
import { AppRoutingModule } from './app-routing.module';
import { CreateEmployeeComponent } from '../components/create-employee/create-employee.component';
import { CreateQualificationComponent } from '../components/create-qualification/create-qualification.component';
import {AuthserviceService} from "./authservice.service";
import { UpdateEmployeeComponent } from '../components/update-employee/update-employee.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    ControlPanelComponent,
    EmployeeListComponent,
    QualificationListComponent,
    LoginComponent,
    QualificationListComponent,
    EmployeeDetailComponent,
    CreateEmployeeComponent,
    CreateQualificationComponent,
    UpdateEmployeeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    AppComponent,
    AuthserviceService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
