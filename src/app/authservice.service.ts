import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthserviceService {

  constructor() { }
  getBearer(){
    return !!localStorage.getItem("data")
  }
}
