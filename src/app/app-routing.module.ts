import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EmployeeDetailComponent} from "../components/employee-detail/employee-detail.component";
import {QualificationListComponent} from "../components/qualification-list/qualification-list.component";
import {EmployeeListComponent} from "../components/employee-list/employee-list.component";
import {LoginComponent} from "../components/login/login.component";
import {CreateEmployeeComponent} from "../components/create-employee/create-employee.component";
import {AuthguardGuard} from "./authguard.guard";
import {UpdateEmployeeComponent} from "../components/update-employee/update-employee.component";

const routes: Routes = [
  { path: 'employee/:id', component: EmployeeDetailComponent, canActivate:[AuthguardGuard] },
  { path: 'create-employee', component: CreateEmployeeComponent, canActivate:[AuthguardGuard] },
  { path: 'qualifications', component: QualificationListComponent, canActivate:[AuthguardGuard] },
  { path: 'employee-list', component: EmployeeListComponent, canActivate:[AuthguardGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'update-employee/:id', component: UpdateEmployeeComponent, canActivate:[AuthguardGuard] },
  { path: '', redirectTo: 'employee-list', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
